import MinimistConfig from '@mcoffin/minimist-config';
import { stdin } from 'process';
import _ from 'lodash';
import fs from 'fs';
import * as readline from 'node:readline/promises';
import { Readable, Transform, Duplex } from 'stream';
import mcoffinStreamUtils from '@mcoffin/stream-utils';
const { FilterTransform, MapTransform } = mcoffinStreamUtils;
import Lazy from 'lazy.js';
import YAML from 'yaml';
import { mutated, isNullOrUndefined, splitOnce, readFullFileString } from './util.js';

class RequiredArgumentError extends Error {
    constructor(name) {
        super(`Missing required argument: ${name}`);
        this.name = name;
    }
}

class Config extends MinimistConfig {
    constructor(args) {
        super({
            string: ['input'],
            alias: {
                input: ['f', 'i'],
            },
        }, args);
    }

    get inputFile() {
        const v = this.get('input');
        if (_.isArray(v)) {
            if (ret.length < 1 || isNullOrUndefined(ret[0])) {
                return undefined;
            }
            return ret[0];
        }
        return v;
    }

    get funds() {
        let v = this.trailing;
        if (v.length < 1 || isNullOrUndefined(v[0])) {
            throw new RequiredArgumentError('FUNDS');
        }
        if (_.isString(v[0])) {
            return parseInt(v[0]);
        }
        return v[0];
    }

    get inputStream() {
        const inputFile = this.inputFile;
        if (!isNullOrUndefined(inputFile)) {
            return fs.createReadStream(inputFile, { encoding: 'utf8' });
        } else {
            return stdin;
        }
    }
}

class LineStream extends Transform {
    constructor(options = {}) {
        super(_.merge(options, {
            readableObjectMode: true,
            writableObjectMode: false,
        }));
        this.buffer = "";
    }

    _transform(chunk, encoding, callback) {
        if (chunk instanceof Buffer) {
            chunk = chunk.toString('utf8');
        }
        this.buffer = this.buffer + chunk;
        const lines = [];
        while (this.buffer.indexOf("\n") >= 0) {
            const [line, remaining] = splitOnce(this.buffer, this.buffer.indexOf("\n"));
            lines.push(line);
            this.buffer = remaining;
        }
        if (lines.length > 0) {
            callback(null, lines);
        } else {
            callback();
        }
    }
}

class ParseCsv extends Transform {
    constructor(delimeter = ",", options = {}) {
        super(_.merge(options, {
            readableObjectMode: true,
            writableObjectMode: true
        }));
        this.delimeter = delimeter;
    }

    _transform(lines, encoding, callback) {
        callback(null, lines.map((line) => line.split(this.delimeter)));
    }
}

async function main(config) {
    const blacklist = await readFullFileString('blacklist.yml')
        .then((s) => YAML.parse(s));
    const s = config.inputStream
        .pipe(new LineStream())
        .pipe(new FilterTransform((lines) => lines.length > 0))
        .pipe(new MapTransform((lines) => _.filter(lines, (line) => line.length > 0)))
        .pipe(new ParseCsv())
        .pipe(new MapTransform((lines) => _.filter(lines, ([line]) => !line.startsWith('Who would') && !blacklist.names.includes(line))))
        .pipe(new MapTransform((lines) => {
            const positiveLines = Lazy(lines)
                .map(([ts, sender, person]) => person)
                .map((name) => {
                    return {
                        name,
                        points: 1,
                    };
                });
            const losses = {};
            Lazy(lines)
                .filter((line) => _.isString(line[7]) && line[7].length > 0 && line[7] != "Name:")
                .map((line) => [line[7], _.isString(line[8]) ? parseInt(line[8]) : line[8]])
                .each(([name, lost]) => {
                    losses[name] = (losses[name] || 0) + lost;
                });
            return Lazy(losses)
                .pairs()
                .concat(Lazy(blacklist.losses).pairs())
                .map(([name, lost]) => {
                    return {
                        name,
                        points: 0 - lost,
                    };
                })
                .concat(positiveLines)
                .map(mutated((v) => {
                    const { name } = v;
                    v.name = blacklist.alias[name] || name;
                }))
                .filter(({ name }) => {
                    if (!_.isString(name)) {
                        return false;
                    }
                    if (blacklist.names.includes(name)) {
                        return false;
                    }
                    if (name.startsWith('Who would') || name.startsWith('Manually')) {
                        return false;
                    }
                    return true;
                })
                .toArray();
        }))
    const [pointsMap, splitPoints] = await new Promise((resolve, reject) => {
        const people = {};
        const splitPoints = [];
        s.on('data', (lines) => lines.forEach((o) => {
            const { name, points } = o;
            splitPoints.push(o);
            if (!_.isNumber(people[name])) {
                people[name] = 0;
            }
            people[name] += points;
        }));
        s.on('error', (e) => reject(e));
        s.on('end', () => resolve([people, splitPoints]));
    })
        .then(([points, splitPoints]) => {
            points = Lazy(points)
                .pairs()
                .filter(([k, v]) => _.isNumber(v) && v > 0)
                .toObject();
            return [points, splitPoints];
        });

    const totalPoints = Lazy(pointsMap)
        .pairs()
        .map(([name, points]) => points)
        .sum();

    let ret = Lazy(pointsMap)
        .pairs()
        .map(([name, points]) => [name, {
            points,
            money: Math.trunc((points / (totalPoints + 0.0)) * config.funds),
        }])
        .toObject();
    ret = {
        funds: config.funds,
        total: totalPoints,
        pointValue: Math.trunc((1.0 / totalPoints) * config.funds),
        people: ret,
        entries: splitPoints
    };
    console.log(JSON.stringify(ret, null, 4));
}

const config = new Config(process.argv.slice(2));
main(config);
