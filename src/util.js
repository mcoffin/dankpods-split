import _ from 'lodash';
import fs from 'fs';

export function readFullFileString(p, encoding = 'utf8') {
    const s = fs.createReadStream(p, { encoding });
    return new Promise((resolve, reject) => {
        let buf = '';
        s.on('data', (chunk) => {
            if (chunk instanceof Buffer) {
                chunk = chunk.toString(encoding);
            }
            buf = buf + chunk;
        });
        s.on('error', (e) => reject(e));
        s.on('end', () => resolve(buf));
    });
}

export function splitOnce(s, idx) {
    let end = null;
    if (s.length <= (idx + 1)) {
        end = "";
    } else {
        end = s.slice(idx + 1);
    }
    const start = idx == 0 ? "" : s.slice(0, idx);
    return [start, end];
}

export function isNullOrUndefined(v) {
    return _.isUndefined(v) || _.isNull(v);
}

export function mutated(f) {
    return (v) => {
        f(v);
        return v;
    };
}
